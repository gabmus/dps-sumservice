package org.gabmus.addservice;

import static java.lang.System.exit;

public class ErrorHandler {
    public static void disgraceExit(Throwable e, String message) {
        System.err.println(message);
        e.printStackTrace();
        exit(1);
    }
}
