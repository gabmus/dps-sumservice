package org.gabmus.addservice.tcp.client;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class ClientMain {
    public static void main(String[] argv) {

        Scanner inScanner = new Scanner(System.in);

        while (true) {
            int port = -1;

            while (port == -1) {
                System.out.print("port>>> ");
                port = inScanner.nextInt();
                if (port < 1024 || port > 65535) {
                    System.out.println("You need to provide a port between 1024 and 65535");
                    port = -1;
                }
            }
            try {
                System.out.print("address>>> ");
                String address = inScanner.next();
                Socket clientSocket = new Socket(address, port);

                System.out.println("Provide two integers");
                String arg1 = Integer.toString(inScanner.nextInt());
                String arg2 = Integer.toString(inScanner.nextInt());
                // send request, break at the end

                PrintStream toServer = new PrintStream(clientSocket.getOutputStream());
                Scanner fromServerScanner = new Scanner(clientSocket.getInputStream());

                toServer.println(arg1+";"+arg2);
                System.out.println(fromServerScanner.nextInt());
                fromServerScanner.close();
                clientSocket.close();

                break;
            }
            catch (IOException e) {
                System.err.println("Connection error, retrying");
            }
            inScanner.close();
        }
    }
}