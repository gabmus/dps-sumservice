package org.gabmus.addservice.tcp.server;

import org.gabmus.addservice.ErrorHandler;
import org.gabmus.addservice.ServerWorker;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ServerMain {

    public static void main(String[] argv) {

        int port = -1;

        while (port == -1) {
            System.out.print("port>>> ");
            Scanner inScanner = new Scanner(System.in);
            port = inScanner.nextInt();
            if (port < 1024 || port > 65535) {
                System.out.println("You need to provide a port between 1024 and 65535");
                port = -1;
            }
            inScanner.close();
        }

        ServerSocket listeningSocket = null;
        try {
            listeningSocket = new ServerSocket(port);
        } catch (IOException e) {
            ErrorHandler.disgraceExit(e, "Error occurred in server socket creation. Terminating");
        }

        while (true) {
            try {
                Socket connectionSocket = listeningSocket.accept();
                System.out.println("Connecting to client `"+
                        connectionSocket.getInetAddress()+
                        "` on port `"+connectionSocket.getPort()+"`"
                );
                BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                PrintStream outToClient = new PrintStream(connectionSocket.getOutputStream());
                outToClient.println(ServerWorker.sum(inFromClient.readLine()));
                connectionSocket.close();
            }
            catch (IOException e) {
                ErrorHandler.disgraceExit(e, "Error occurred in handling connection. Terminating");
            }
        }

    }
}
