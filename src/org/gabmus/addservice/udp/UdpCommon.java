package org.gabmus.addservice.udp;

import org.gabmus.addservice.ErrorHandler;

import javax.xml.crypto.Data;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpCommon {

    public static DatagramPacket makePacket(int size) {
        return new DatagramPacket(new byte[size], size);
    }

    public static DatagramPacket makePacket(String message) {
        return new DatagramPacket(message.getBytes(), message.length());
    }

    public static DatagramPacket makePacket(String message, InetAddress targetAddr, int targetPort) {
        return new DatagramPacket(message.getBytes(), message.length(), targetAddr, targetPort);
    }

    public static String packetToString(DatagramPacket packet) {
        return new String(packet.getData(), 0, packet.getLength());
    }

    public static String getMessageAndPacket(DatagramSocket socket) {
        DatagramPacket packet = makePacket(1024);
        try {
            socket.receive(packet);
            return packetToString(packet);
        }
        catch (IOException e) {
            ErrorHandler.disgraceExit(e, "Error receiving packet. Terminating");
        }
        return "";
    }

    public static void sendMessage(DatagramSocket socket, String message, InetAddress targetAddr, int targetPort) {
        DatagramPacket packet = makePacket(message, targetAddr, targetPort);
        try {
            socket.send(packet);
        }
        catch (IOException e) {
            ErrorHandler.disgraceExit(e, "Error sending packet. Terminating");
        }
    }
}
