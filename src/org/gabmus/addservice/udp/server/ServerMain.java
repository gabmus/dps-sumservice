package org.gabmus.addservice.udp.server;

import org.gabmus.addservice.ErrorHandler;
import org.gabmus.addservice.ServerWorker;
import org.gabmus.addservice.udp.UdpCommon;

import javax.xml.crypto.Data;
import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ServerMain {

    public static void main(String[] argv) {

        int port = -1;

        while (port == -1) {
            System.out.print("port>>> ");
            Scanner inScanner = new Scanner(System.in);
            port = inScanner.nextInt();
            if (port < 1024 || port > 65535) {
                System.out.println("You need to provide a port between 1024 and 65535");
                port = -1;
            }
            inScanner.close();
        }

        while (true) {
            try {
                DatagramSocket listeningSocket = new DatagramSocket(port);
                DatagramPacket receivePacket = UdpCommon.makePacket(1024);
                listeningSocket.receive(receivePacket);
                System.out.println("Connecting to client `"+
                        receivePacket.getAddress()+
                        "` on port `"+receivePacket.getPort()+"`"
                );
                UdpCommon.sendMessage(
                        listeningSocket,
                        ServerWorker.sum(UdpCommon.packetToString(receivePacket)),
                        receivePacket.getAddress(),
                        receivePacket.getPort()
                );
                listeningSocket.close();
            }
            catch (IOException e) {
                ErrorHandler.disgraceExit(e, "Error occurred in handling connection. Terminating");
            }
        }

    }
}
