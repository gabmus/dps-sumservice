package org.gabmus.addservice.udp.client;

import org.gabmus.addservice.udp.UdpCommon;

import java.io.IOException;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class ClientMain {
    public static void main(String[] argv) {

        Scanner inScanner = new Scanner(System.in);

        while (true) {
            int port = -1;

            while (port == -1) {
                System.out.print("port>>> ");
                port = inScanner.nextInt();
                if (port < 1024 || port > 65535) {
                    System.out.println("You need to provide a port between 1024 and 65535");
                    port = -1;
                }
            }
            try {
                System.out.print("address>>> ");
                String address = inScanner.next();
                DatagramSocket clientSocket = new DatagramSocket();

                System.out.println("Provide two integers");
                String arg1 = Integer.toString(inScanner.nextInt());
                String arg2 = Integer.toString(inScanner.nextInt());
                // send request, break at the end

                UdpCommon.sendMessage(clientSocket, arg1 + ";" + arg2, InetAddress.getByName(address), port);
                DatagramPacket receivePacket = UdpCommon.makePacket(1024);
                clientSocket.receive(receivePacket);
                System.out.println(UdpCommon.packetToString(receivePacket));

                clientSocket.close();

                break;
            } catch (IOException e) {
                e.printStackTrace();
                System.err.println("Connection error, retrying");
            }
        }
        inScanner.close();
    }
}