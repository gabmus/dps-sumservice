package org.gabmus.addservice;

public class ServerWorker {

    public static String sum(String message) {
        int total = 0;
        for (String op: message.split(";")) {
            total += Integer.parseInt(op);
        }
        return Integer.toString(total);
    }

}
